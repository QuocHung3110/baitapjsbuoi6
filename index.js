// start BT1
function findMinN() {
  var sum = 0;
  var n = 0;
  while (sum <= 10000) {
    n++;
    sum = sum + n;
  }
  document.getElementById(
    "findMinN"
  ).innerHTML = ` Số nguyên dương nhỏ nhất: <b>${n}</b>`;
}
// end BT1

// start BT2
function sumMathPow() {
  var n = document.getElementById("n").value * 1;
  if (n <= 0 || Number.isInteger(n) == false) {
    return alert("Vui lòng nhập lại số n NGUYÊN DƯƠNG và >=1");
  }
  var x = document.getElementById("x").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum = sum + Math.pow(x, i);
  }
  document.getElementById("sumMathPow").innerHTML = `Tổng: <b>${sum}</b> `;
}
// end BT2

// start BT3
function factorialCalculation() {
  var n2 = document.getElementById("n2").value * 1;
  if (n2 <= 0 || Number.isInteger(n2) == false) {
    return alert("Vui lòng nhập lại số n NGUYÊN DƯƠNG và >=1");
  }
  // xét trường hợp ko hợp lệ
  var multiply = 1;
  for (var i = 1; i <= n2; i++) {
    console.log(i);
    multiply = multiply * i;
    console.log(multiply);
  }
  document.getElementById(
    "factorialCalculation"
  ).innerHTML = `Giai thừa: <b>${multiply}</b>`;
}
// end BT3

// start BT4
function createDiv() {
  var result = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      result =
        result +
        ' <div class="col-12" style="background-color:#0B4B72;height:30px;">Div chẵn</div>';
    } else {
      result =
        result +
        ' <div class="col-12" style="background-color:#FCD716;height:30px;color:black;">Div lẻ</div>';
    }
  }
  document.getElementById("createDiv").innerHTML = `${result}`;
}
// end BT4
